import urllib2
import rdflib

def ReadSource( url ):
    print( "" )
    print( "" )
    print( "--------------------------------------------------" )
    print( "--------------------------------------------------" )
    print( "Reading " + url + "..." )
    graph = rdflib.Graph()

    result = graph.parse( url )

    print( result )

    count = 0
    for subject, predicate, object in graph:
        print( "Item " + str( count ) + ":" )
        print( "\t Subject: " + subject )
        print( "\t Predicate: " + predicate )
        print( "\t Object: " + object )
    
        count += 1

ReadSource( "http://www.w3.org/People/Berners-Lee/card" )
ReadSource( "https://rejcx.solid.community/profile/card" )    
